FROM nvidia/cuda:10.2-cudnn7-devel-ubuntu18.04 AS build

ARG OPENCV_VERSION=4.2.0
ARG FFMPEG_VERSION=4.2.1

ENV NVIDIA_DRIVER_CAPABILITIES=compute,video,utility 

WORKDIR /build

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends build-essential curl cmake git yasm \
    libgstreamer-plugins-base1.0-dev libgstreamer1.0-dev gettext  libtool autoconf autopoint automake \
    libfreetype6-dev libfreetype6 unzip nasm libharfbuzz-bin libharfbuzz-dev \
    libnuma-dev python3-dev python-dev libeigen3-dev python-numpy python3-numpy

RUN git clone https://github.com/FFmpeg/nv-codec-headers.git && cd nv-codec-headers && \
    make && make install && cd .. && \
    git clone https://code.videolan.org/videolan/x264.git --depth=1 && \
    cd x264 && ./configure --prefix=/usr/local/x264 --disable-cli --enable-static --enable-shared && \
    make -j$(nproc) && make install && cp -R /usr/local/x264/lib/pkgconfig/*.pc /usr/local/lib/pkgconfig && \
    echo "/usr/local/x264/lib" > /etc/ld.so.conf.d/x-codecs.conf && ldconfig && cd .. && \
    git clone https://github.com/videolan/x265.git && \
    cd x265/build/linux && cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local/x265 -D ENABLE_SHARED=ON ../../source/ && \
    make -j$(nproc) && make install &&  cp -R /usr/local/x265/lib/pkgconfig/*.pc /usr/local/lib/pkgconfig && \
    echo "/usr/local/x265/lib" >> /etc/ld.so.conf.d/x-codecs.conf && ldconfig && cd .. && \
    curl -s https://ffmpeg.org/releases/ffmpeg-${FFMPEG_VERSION}.tar.bz2 | tar xj && cd ffmpeg-${FFMPEG_VERSION} && \
    ./configure --enable-cuda-nvcc --enable-cuda --enable-cuvid --enable-nvenc --enable-nonfree --enable-libnpp \
    --enable-avresample --enable-swresample --enable-libx264 --enable-libx265 --enable-gpl extra-cflags=-I/usr/local/cuda/include \
    --extra-ldflags=-L/usr/local/cuda/lib64 --prefix=/usr/local/ffmpeg-${FFMPEG_VERSION} --enable-shared && \
    make -j$(nproc) && make install && cp -R /usr/local/ffmpeg-${FFMPEG_VERSION}/lib/pkgconfig/*.pc /usr/local/lib/pkgconfig && \
    ln -sf /usr/local/ffmpeg-${FFMPEG_VERSION} /usr/local/ffmpeg && \
    echo "/usr/local/ffmpeg/lib" > /etc/ld.so.conf.d/ffmpeg.conf && ldconfig && cd .. && \
    git clone https://gitlab.freedesktop.org/gstreamer/gst-plugins-bad.git && \
    cd gst-plugins-bad &&  git checkout tags/1.14.5  && \
    ./autogen.sh --disable-gtk-doc --enable-x265 && make -j$(nproc) && make install

RUN git clone https://github.com/opencv/opencv.git --depth=1 && \
    git clone https://github.com/opencv/opencv_contrib.git --depth=1 && \
    mkdir opencv_build && cd opencv_build && \
    cmake \
    -D CMAKE_BUILD_TYPE=RELEASE \
    -D CMAKE_INSTALL_PREFIX=/usr/local/opencv-${OPENCV_VERSION} \
    -D OPENCV_EXTRA_MODULES_PATH=../opencv_contrib/modules \
    -D EIGEN_INCLUDE_PATH=/usr/include/eigen3 \
    -D OPENCV_ENABLE_NONFREE=ON \
    -D OPENCV_GENERATE_PKGCONFIG=ON \
    -D BUILD_EXAMPLES=OFF \
    -D BUILD_DOCS=OFF \
    -D BUILD_OPENCV_LEGACY=OFF \
    -D BUILD_TESTS=OFF \
    -D BUILD_PERF_TESTS=OFF \
    -D BUILD_OPENCV_PYTHON2=ON \
    -D BUILD_OPENCV_PYTHON3=ON \
    -D WITH_CUDA=ON \
    -D CUDA_ARCH_BIN="6.1 7.0 7.5" \
    -D ENABLE_FAST_MATH=ON \
    -D CUDA_FAST_MATH=ON \
    -D WITH_CUBLAS=ON \
    -D OPENCV_DNN_CUDA=ON \
    -D WITH_NVCUVID=OFF \
    -D VIDEOIO_PLUGIN_LIST=gstreamer,ffmpeg ../opencv && \
    make -j$(nproc) && make install && \
    cp -R /usr/local/opencv-${OPENCV_VERSION}/lib/pkgconfig/*.pc /usr/local/lib/pkgconfig

FROM nvidia/cuda:10.2-cudnn7-runtime-ubuntu18.04

ARG OPENCV_VERSION=4.2.0
ARG FFMPEG_VERSION=4.2.1

COPY --from=build /usr/local/lib /usr/local/lib
COPY --from=build /usr/local/include /usr/local/include
COPY --from=build /usr/local/x264 /usr/local/x264
COPY --from=build /usr/local/x265 /usr/local/x265
COPY --from=build /usr/local/ffmpeg-${FFMPEG_VERSION} /usr/local/ffmpeg-${FFMPEG_VERSION}
COPY --from=build /usr/local/opencv-${OPENCV_VERSION} /usr/local/opencv-${OPENCV_VERSION}
COPY mhier-ubuntu-libboost-latest-bionic.list /etc/apt/sources.list.d/
COPY gstreamer-1.0 /usr/local/lib/gstreamer-1.0

ENV NVIDIA_DRIVER_CAPABILITIES=compute,video,utility \
    APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE=1 \
    GST_PLUGIN_PATH=$GST_PLUGIN_PATH:/usr/local/lib/gstreamer-1.0 \
    PATH=$PATH:/usr/local/opencv/bin:/usr/local/ffmpeg/bin \
    LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/ffmpeg/lib \
    OPENCV_FFMPEG_CAPTURE_OPTIONS="video_codec;h264_cuvid|rtsp_transport;tcp"

RUN mkdir ~/.gnupg || echo "disable-ipv6" >> ~/.gnupg/dirmngr.conf && \
    apt-key adv --recv-keys 31F54F3E108EAD31 && apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends software-properties-common && \
    add-apt-repository ppa:ubuntu-toolchain-r/test -y && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
    libgcc1 libstdc++6 libboost1.68 libfreetype6 libharfbuzz-bin python-numpy python3-numpy libnuma1 libzmq5 \
    gstreamer1.0-rtsp gstreamer1.0-plugins-base-apps gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-gl libxcb-shape0 && \
    ln -sf /usr/local/opencv-${OPENCV_VERSION} /usr/local/opencv && \
    ln -sf /usr/local/ffmpeg-${FFMPEG_VERSION} /usr/local/ffmpeg && \
    apt-get purge software-properties-common -y && apt-get autoremove -y && \
    echo "/usr/local/x264/lib" > /etc/ld.so.conf.d/x-codecs.conf && \
    echo "/usr/local/x265/lib" >> /etc/ld.so.conf.d/x-codecs.conf && \
    echo "/usr/local/ffmpeg/lib" > /etc/ld.so.conf.d/ffmpeg.conf && \
    echo "/usr/local/opencv/lib" > /etc/ld.so.conf.d/opencv.conf && \
    ldconfig && apt-get clean all
